from subprocess import run, PIPE, DEVNULL
import os, re, sys
from pathlib import Path


HERE = os.path.dirname(os.path.abspath(__file__))
my_conf = 'refind-caph1993/theme.conf'

def walk_dir(path):
    q = [path]
    vis = set(q)
    while q:
        path = q.pop()
        for p in os.listdir(path):
            child = path / p
            if os.path.isfile(child):
                yield child
            elif child not in vis: # symlink cycles or double passes
                q.append(child)
                vis.add(child)
    return


def locate_refind_conf():
    print('Searching for refind installation...')
    locs = [f for f in walk_dir(Path('/boot')) if f.name=='refind.conf']
    assert locs, f'Abort. refind is not installed: cannot locate refind.conf in /boot/**'
    locs = {str(i):x for i,x in enumerate(locs)}
    i = '' if len(locs)>1 else '0'
    while i not in locs:
        print()
        print('Several refind locations found. Choose the main one:\n')
        for k,v in locs.items():
            print(f'    [{k}] {v}')
        print('    [ctrl+c] abort installation\n')
        i = input('Type your [choice]: ').strip()
    conf = locs[i]
    print(f'  Using installation at {conf}')
    return conf


def edit_lines(lines, my_conf, uninstall=False):
    added = 0
    for line in lines:
        m = re.match(r'(#?)include themes/(.*) *', line)
        if m:
            comment = m.group(1)=='#'
            theme_conf = m.group(2)
            if theme_conf==my_conf:
                if uninstall:
                    continue
                if comment:
                    line = line[1:]
                added = 1
            elif not comment and not uninstall:
                line = '#'+line
        yield line.rstrip()
    if not uninstall and not added:
        yield f'include themes/{my_conf}'
    return


def edit_conf(conf, my_conf, uninstall=False):
    with open(conf) as f: lines = f.readlines()
    ready = '\n'.join(edit_lines(lines, my_conf, uninstall))
    with open(conf, 'w') as f: f.write(ready)
    return

def uninstall():
    try:
        edit_conf(conf, my_conf, uninstall=True)
    except PermissionError:
        print('\nERROR. Permission denied. Please run with sudo')
        sys.exit(1)
    run(['mkdir', '-p', themes+'/refind-caph1993'], check=1)
    run(['rm', '-r', themes+'/refind-caph1993'], check=1)
    print('ok')

def web_install():
    print('Downloading the latest script for (un)installation')
    tmp_zip = 'tmp_zip-123123123r12123.zip'
    master = 'https://bitbucket.org/caph1993/refind-caph1993/get/master.zip'
    with open(tmp_zip, 'wb') as tmp_file:
        run(['curl', master], stdout=tmp_file)

    try:
        out = run(['unzip', '-l', tmp_zip], stdout=PIPE).stdout.decode()
        folder = re.findall(r'(caph1993-.*?)\/', out)[0]
        run(['unzip', tmp_zip])
    finally:
        run(['rm', tmp_zip], check=True)

    print('\nok\nInstalling theme...\n')
    try:
        run(['sudo', 'python3', 'installer.py', 'install'], cwd=folder, check=True)
    except:
        pass
    run(['rm', '-r', folder], check=True)
    return


if __name__=='__main__':
    _, *args =  sys.argv
    if args == []:
        ans = input('Type 1 for installing or 0 for uninstalling: ').strip()
        assert ans in ['0', '1'], f'Invalid option'
        args = ['uninstall' if ans=='0' else 'web-install']
        
    if  args!=['install'] and args!=['uninstall'] and args!=['web-install']:
        print(
            'Run one of'
            '\n    python3 installer.py'
            '\n    (sudo) python3 installer.py uninstall'
            '\n    (sudo) python3 installer.py install'
            '\n    (sudo) python3 installer.py web-install'
        )
        sys.exit(1)

    conf = locate_refind_conf()
    refind = os.path.dirname(conf)
    themes = refind+'/themes'
    print(f'Themes location: {themes}')
    run(['mkdir', '-p', themes], check=1)
    

    if args==['web-install']:
        web_install()
    elif args==['uninstall']:
        print('Uninstalling...')
        uninstall()
    else:
        print('Uninstalling any previous version...')
        uninstall()
        print('Installing...')
        run(['cp', '-r', HERE, themes], check=1)
        edit_conf(conf, my_conf)
        print('ok')
