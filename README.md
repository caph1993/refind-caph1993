# refind-caph1993
## Yet another rEFInd theme

This is just a mere modified fork of the rEFInd-glassy theme.

Changes:

 + Added installer.py for easy install/uninstall
 + Background was changed to a nice plain color that loads fast.
 + Several icons were removed.
 + Fallback bootloaders were hidden.


Install/uninstall with:

 + `(sudo) python3 installer.py install`
 + `(sudo) python3 installer.py uninstall`


Screenshot:

![](screenshot.png)
